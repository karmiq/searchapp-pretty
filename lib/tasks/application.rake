namespace :app do
  desc "Bootstrap the application"
  task :bootstrap do
    Rake::Task['db:reset'].invoke
  end
end
